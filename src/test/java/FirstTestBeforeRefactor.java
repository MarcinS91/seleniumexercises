import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class FirstTestBeforeRefactor {
    public static void main(String[] args) {
        System.setProperty("webdriver.gecko.driver", "src/resources/geckodriver");
        String pageTitle = "";
        WebDriver driver = new FirefoxDriver();
        driver.get("http://automationpractice.com/index.php");
        pageTitle = driver.getTitle();
        System.out.println(pageTitle);
        if (pageTitle.equals("My Store")) {
            System.out.println("Test pozytywny");
        } else {
            System.out.println("Test negatywny");
        }
        driver.close();

    }
}
