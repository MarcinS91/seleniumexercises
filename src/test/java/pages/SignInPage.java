package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class SignInPage {

    @FindBy(id = "SubmitLogin")
    WebElement signIn;

    @FindBy(id = "SubmitCreate")
    WebElement createAccount;

    @FindBy(id = "email_create")
    WebElement emailAddressCreate;

    @FindBy(id = "email")
    WebElement email;

    @FindBy(id = "passwd")
    WebElement password;

    @FindBy(id = "create_account_error")
    WebElement accountError;

    @FindBy(className = "info-account")
    WebElement title;

    public String getTitle() {
        return title.getText();
    }

    public SignInPage(WebDriver driver) {

        PageFactory.initElements(driver, this);
    }

    public void doLogin(String mail, String pass) {
        email.sendKeys(mail);
        password.sendKeys(pass);
        signIn.click();
    }

    public void createAccount(String mail) {
        emailAddressCreate.sendKeys(mail);
        createAccount.click();

    }

    public String getEmailInput() {
        return emailAddressCreate.getAttribute("value");
    }

    public boolean validateAccountError() {
        return accountError.isDisplayed();
    }
}
