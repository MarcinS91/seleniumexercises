import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class LoginTestRefaktor {
    private WebDriver driver;
    private WebElement emailInput;
    private WebElement passwordInput;
    private WebElement submitLogin;
    private String url;
    private String title;

    @BeforeClass
    public void setUp() {
        System.setProperty("webdriver.gecko.driver", "src/resources/geckodriver");
        driver = new FirefoxDriver();
    }

    @Test
    public void doLogin() {
        url = "http://automationpractice.com/index.php?controller=authentication&back=my-account";
        driver.get(url);
        emailInput = driver.findElement(By.id("email"));
        emailInput.sendKeys("poczta@poczta.pl");
        passwordInput = driver.findElement(By.id("passwd"));
        passwordInput.sendKeys("password");
        submitLogin = driver.findElement(By.id("SubmitLogin"));
        submitLogin.click();
        title = driver.findElement(By.className("info-account")).getText();
        Assert.assertEquals("Welcome to your account. Here you can manage all of your personal information and orders.", title);
    }

    @AfterClass
    public void tearDown() {
        driver.quit();
    }
}
