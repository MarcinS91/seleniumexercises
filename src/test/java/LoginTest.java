import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.*;

public class LoginTest {
    private WebDriver driver;

    @BeforeTest
    public void setUp() {
        System.setProperty("webdriver.gecko.driver", "src/resources/geckodriver");
        driver = new FirefoxDriver();
    }

    @Test
    public void doLogin() {
        String url = "http://automationpractice.com/index.php?controller=authentication&back=my-account";
        driver.get(url);
        driver.findElement(By.id("email")).sendKeys("poczta@poczta.pl");
        driver.findElement(By.id("passwd")).sendKeys("password");
        driver.findElement(By.id("SubmitLogin")).click();
        String title = driver.findElement(By.className("info-account")).getText();
        Assert.assertEquals("Welcome to your account. Here you can manage all of your personal information and orders.", title);
    }

   @AfterTest
    public void tearDown() {
        driver.quit();
    }
}
