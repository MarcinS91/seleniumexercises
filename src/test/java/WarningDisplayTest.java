import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.*;


public class WarningDisplayTest {
    private WebDriver driver;

    private WebElement emailInput;

    private boolean warning;

    private WebElement createAccountButton;

    private String url;

    @BeforeTest
    public void setUp() {
        System.setProperty("webdriver.gecko.driver", "src/resources/geckodriver");
        driver = new FirefoxDriver();
    }

    @Test
    public void tryToLogin() {
        url = "http://automationpractice.com/index.php?controller=authentication&back=my-account";
        driver.get(url);
        emailInput = driver.findElement(By.id("email_create"));
        emailInput.sendKeys("poczta@poczta.pl");
        createAccountButton = driver.findElement(By.id("SubmitCreate"));
        createAccountButton.submit();

        WebDriverWait wait = new WebDriverWait(driver, 5);
        wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.id("create_account_error"))));
        warning = driver.findElement(By.id("create_account_error")).isDisplayed();

        Assert.assertTrue(warning);
    }

    @AfterTest
    public void tearDown() {
        driver.quit();
    }
}
