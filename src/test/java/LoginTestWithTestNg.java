import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.*;
import pages.SignInPage;

public class LoginTestWithTestNg {

    private WebDriver driver;
    private String url;

    SignInPage signInPage;

    @BeforeTest
    public void setUp() {
        System.setProperty("webdriver.gecko.driver", "src/resources/geckodriver");
        driver = new FirefoxDriver();
    }

    @Test
    public void doLogin() {
        url = "http://automationpractice.com/index.php?controller=authentication&back=my-account";
        driver.get(url);
        signInPage = new SignInPage(driver);
        signInPage.doLogin("poczta@poczta.pl", "password");
        Assert.assertEquals(signInPage.getTitle(), "Welcome to your account. Here you can manage all of your personal information and orders.");
    }

    @AfterClass
    public void tearDown() {
        driver.quit();
    }
}
