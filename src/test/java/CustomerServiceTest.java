import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.*;

import java.io.File;

public class CustomerServiceTest {

    private WebDriver driver;

    private String url;

    private WebElement contactUsButton;

    private WebElement messageTextbox;

    private WebElement sendButton;

    private WebElement inputFile;

    private WebElement emailAddressInput;

    private WebElement orderReferenceInput;

    private Select subjectHeading;

    private boolean success;

    @BeforeTest
    public void setUp() {
        System.setProperty("webdriver.gecko.driver", "src/resources/geckodriver");
        driver = new FirefoxDriver();
    }

    @Test
    public void sendMessage() {
        url = "http://automationpractice.com/index.php";
        driver.get(url);
        contactUsButton = driver.findElement(By.xpath("//a[@href='http://automationpractice.com/index.php?controller=contact']"));
        contactUsButton.click();
        subjectHeading = new Select(driver.findElement(By.id("id_contact")));
        subjectHeading.selectByVisibleText("Customer service");
        emailAddressInput = driver.findElement(By.id("email"));
        emailAddressInput.sendKeys("poczta@poczta.pl");
        orderReferenceInput = driver.findElement(By.id("id_order"));
        orderReferenceInput.sendKeys("Test");
        File file = new File("src/resources/test.txt");
        inputFile = driver.findElement(By.id("fileUpload"));
        inputFile.sendKeys(file.getAbsolutePath());
        messageTextbox = driver.findElement(By.id("message"));
        messageTextbox.sendKeys("Test");
        sendButton = driver.findElement(By.id("submitMessage"));
        sendButton.click();
        success = driver.findElement(By.xpath("//p[@class='alert alert-success']")).isDisplayed();
        Assert.assertTrue(success);
    }

    @AfterTest
    public void tearDown() {
        driver.quit();
    }
}
