import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.util.List;

public class DressCountTest {

    private WebDriver driver;

    private WebElement searchInput;

    private WebElement searchButton;

    private String url;

    private List<WebElement> dresses;

    @BeforeTest
    public void setUp() {
        System.setProperty("webdriver.gecko.driver", "src/resources/geckodriver");
        driver = new FirefoxDriver();
    }

    @Test
    public void countDress() {
        url = "http://automationpractice.com/index.php";
        driver.get(url);
        searchInput = driver.findElement(By.id("search_query_top"));
        searchInput.sendKeys("DRESS");
        searchButton = driver.findElement(By.name("submit_search"));
        searchButton.submit();

        WebDriverWait wait = new WebDriverWait(driver, 5);
        wait.until(ExpectedConditions.titleIs("Search - My Store"));
        dresses = driver.findElements(By.xpath("//div[@class='product-image-container']"));
        System.out.println(dresses.size());

    }

    @AfterTest(alwaysRun = true)
    public void shutdown() {
        driver.quit();
    }
}
