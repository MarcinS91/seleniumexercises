import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import pages.NavigationPanelPage;
import pages.SearchResultPage;

public class DressCountTestNgTest {

    private WebDriver driver;
    private String url;

    NavigationPanelPage navigationPanelPage;
    SearchResultPage searchResultPage;

    @BeforeTest
    public void setUp() {
        System.setProperty("webdriver.gecko.driver", "src/resources/geckodriver");
        driver = new FirefoxDriver();
    }

    @Test
    public void dressCount() {
        url = "http://automationpractice.com/index.php";
        driver.get(url);
        navigationPanelPage = new NavigationPanelPage(driver);
        navigationPanelPage.searchDress("DRESS");
        searchResultPage = new SearchResultPage(driver);
        System.out.println(searchResultPage.count());
    }

    @AfterClass
    public void tearDown() {
        driver.quit();
    }
}
