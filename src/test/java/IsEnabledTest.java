import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.*;

public class IsEnabledTest {
    private WebDriver driver;
    private WebElement emailInput;
    private WebElement passwordInput;
    private WebElement submitLogin;
    private String url;
    private boolean signOutButton;

    @BeforeTest
    public void setUp() {
        System.setProperty("webdriver.gecko.driver", "src/resources/geckodriver");
        driver = new FirefoxDriver();
    }

    @Test
    public void doLogin() {
        url = "http://automationpractice.com/index.php?controller=authentication&back=my-account";
        driver.get(url);
        emailInput = driver.findElement(By.id("email"));
        emailInput.sendKeys("poczta@poczta.pl");
        passwordInput = driver.findElement(By.id("passwd"));
        passwordInput.sendKeys("password");
        submitLogin = driver.findElement(By.id("SubmitLogin"));
        submitLogin.click();
        WebDriverWait wait = new WebDriverWait(driver, 5);
        wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.className("logout"))));
        signOutButton = driver.findElement(By.className("logout")).isEnabled();
        Assert.assertTrue(signOutButton);
    }

    @AfterTest
    public void tearDown() {
        driver.quit();
    }
}
